﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace pasha_g
{
    public partial class P1 : Form
    {
        public P1()
        {
            InitializeComponent();
        }

        private void tb_milli_TextChanged(object sender, EventArgs e)
        {
            double value;

            try
            {
                value = Convert.ToDouble(tb_milli.Text);

                if (value < 0) throw new Exception("Отрицательное значение");

                tb_akri.Text = (value * 640).ToString();
                tb_rudi.Text = (value * 4 * 640).ToString();

                status.Text = "Готов";
            }
            catch (Exception error)
            {
                status.Text = "Милли2: " + error.Message;
            }
        }

        private void tb_akri_TextChanged(object sender, EventArgs e)
        {
            double value;

            try
            {
                value = Convert.ToDouble(tb_akri.Text);

                if (value < 0) throw new Exception("Отрицательное значение");

                tb_milli.Text = (value / 640).ToString();
                tb_rudi.Text = (value * 4).ToString();

                status.Text = "Готов";
            }
            catch (Exception error)
            {
                status.Text = "Акры: " + error.Message;
            }
        }

        private void tb_rudi_TextChanged(object sender, EventArgs e)
        {
            double value;

            try
            {
                value = Convert.ToDouble(tb_rudi.Text);

                if (value < 0) throw new Exception("Отрицательное значение");

                tb_akri.Text = (value / 4).ToString();
                tb_milli.Text = (value / (4 * 640)).ToString();

                status.Text = "Готов";
            }
            catch (Exception error)
            {
                status.Text = "Руды: " + error.Message;
            }
        }
    }
}
