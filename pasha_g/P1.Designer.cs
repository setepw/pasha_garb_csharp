﻿namespace pasha_g
{
    partial class P1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tb_milli = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_akri = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_rudi = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.status = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tb_milli
            // 
            this.tb_milli.Location = new System.Drawing.Point(12, 69);
            this.tb_milli.Name = "tb_milli";
            this.tb_milli.Size = new System.Drawing.Size(260, 20);
            this.tb_milli.TabIndex = 0;
            this.tb_milli.TextChanged += new System.EventHandler(this.tb_milli_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Милли2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Акры";
            // 
            // tb_akri
            // 
            this.tb_akri.Location = new System.Drawing.Point(12, 108);
            this.tb_akri.Name = "tb_akri";
            this.tb_akri.Size = new System.Drawing.Size(260, 20);
            this.tb_akri.TabIndex = 2;
            this.tb_akri.TextChanged += new System.EventHandler(this.tb_akri_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 131);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Руды";
            // 
            // tb_rudi
            // 
            this.tb_rudi.Location = new System.Drawing.Point(12, 147);
            this.tb_rudi.Name = "tb_rudi";
            this.tb_rudi.Size = new System.Drawing.Size(260, 20);
            this.tb_rudi.TabIndex = 4;
            this.tb_rudi.TextChanged += new System.EventHandler(this.tb_rudi_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(10, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(208, 25);
            this.label4.TabIndex = 6;
            this.label4.Text = "Милли2-Акры-Руды";
            // 
            // status
            // 
            this.status.AutoSize = true;
            this.status.Location = new System.Drawing.Point(12, 178);
            this.status.Name = "status";
            this.status.Size = new System.Drawing.Size(36, 13);
            this.status.TabIndex = 7;
            this.status.Text = "Готов";
            // 
            // P1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 200);
            this.Controls.Add(this.status);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tb_rudi);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tb_akri);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tb_milli);
            this.Name = "P1";
            this.Text = "P1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb_milli;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_akri;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tb_rudi;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label status;
    }
}