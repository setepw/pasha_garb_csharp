﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace pasha_g
{
    public partial class P5 : Form
    {
        public P5()
        {
            InitializeComponent();
        }

        BufferedGraphics gx;
        BufferedGraphicsContext gx_context;



        
        Point fish_position;
        Point[] bubbles;



        private void P5_Load(object sender, EventArgs e)
        {
            this.Show();

            gx_context = BufferedGraphicsManager.Current;
            gx = gx_context.Allocate(this.CreateGraphics(), this.DisplayRectangle);

            fish_position = new Point(200, 700);

            Random rnd = new Random();
            bubbles = new Point[400];
            for (int i = 0; i < 400; i++)
            {
                bubbles[i] = new Point((int)(rnd.NextDouble() * 600), (int)(200 + rnd.NextDouble() * 800));
            }

            timer_dt.Start();
        }

        private void timer_dt_Tick(object sender, EventArgs e)
        {
            fish_position.Y -= 2;

            if (fish_position.Y < -1000) timer_dt.Stop();

            draw();
        }





        void draw()
        {
            gx.Graphics.Clear(Color.FromArgb(0, 162, 232));

            Brush bubble_brush = new SolidBrush(Color.FromArgb(100, 255, 255, 255));

            for (int i = 10; i < 400; i++)
            {
                gx.Graphics.FillEllipse(bubble_brush, bubbles[i].X, bubbles[i].Y, (int)(1 + 40 / (i/4 + 1)), (int)(1 + 40 / (i/4 + 1)));
                bubbles[i].Y -= 1 + 5 / (i / 10 + 1);
            }

            draw_fish();

            for (int i = 0; i < 10; i++)
            {
                gx.Graphics.FillEllipse(bubble_brush, bubbles[i].X, bubbles[i].Y, (int)(1 + 40 / (i/4 + 1)), (int)(1 + 40 / (i/4 + 1)));
                bubbles[i].Y -= 1 + 5 / (i / 10 + 1);
            }

            gx.Render();
        }

        void draw_fish()
        {
            gx.Graphics.FillEllipse(Brushes.Yellow, fish_position.X-55, fish_position.Y+16, 64, 46);            // Хвост
            gx.Graphics.FillEllipse(Brushes.Yellow, fish_position.X+40, fish_position.Y-10, 40, 30);            // Верхний плавник
            gx.Graphics.FillEllipse(Brushes.Yellow, fish_position.X, fish_position.Y, 160, 80);                 // Тело
            gx.Graphics.DrawEllipse(new Pen(Color.Black, 5), fish_position.X+115, fish_position.Y+20, 20, 16);  // Глаз

            gx.Graphics.DrawString("By Garbuzovsky", this.Font, Brushes.Black, fish_position.X + 10, fish_position.Y + 33);
        }

    }
}
