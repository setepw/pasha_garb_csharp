﻿namespace pasha_g
{
    partial class P3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.display = new System.Windows.Forms.Label();
            this.btn_start = new System.Windows.Forms.Button();
            this.btn_stop = new System.Windows.Forms.Button();
            this.btn_sbros = new System.Windows.Forms.Button();
            this.lb_krugi = new System.Windows.Forms.ListBox();
            this.display_draw_timer = new System.Windows.Forms.Timer(this.components);
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(216, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Секундомер-таймер";
            // 
            // display
            // 
            this.display.BackColor = System.Drawing.Color.Black;
            this.display.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.display.Font = new System.Drawing.Font("Courier New", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.display.ForeColor = System.Drawing.Color.Lime;
            this.display.Location = new System.Drawing.Point(12, 58);
            this.display.Name = "display";
            this.display.Size = new System.Drawing.Size(422, 42);
            this.display.TabIndex = 1;
            this.display.Text = "ERROR";
            this.display.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_start
            // 
            this.btn_start.Location = new System.Drawing.Point(12, 112);
            this.btn_start.Name = "btn_start";
            this.btn_start.Size = new System.Drawing.Size(122, 23);
            this.btn_start.TabIndex = 2;
            this.btn_start.Text = "Старт";
            this.btn_start.UseVisualStyleBackColor = true;
            this.btn_start.Click += new System.EventHandler(this.btn_start_Click);
            // 
            // btn_stop
            // 
            this.btn_stop.Location = new System.Drawing.Point(151, 112);
            this.btn_stop.Name = "btn_stop";
            this.btn_stop.Size = new System.Drawing.Size(138, 23);
            this.btn_stop.TabIndex = 3;
            this.btn_stop.Text = "Стоп";
            this.btn_stop.UseVisualStyleBackColor = true;
            this.btn_stop.Click += new System.EventHandler(this.btn_stop_Click);
            // 
            // btn_sbros
            // 
            this.btn_sbros.Location = new System.Drawing.Point(307, 112);
            this.btn_sbros.Name = "btn_sbros";
            this.btn_sbros.Size = new System.Drawing.Size(127, 23);
            this.btn_sbros.TabIndex = 4;
            this.btn_sbros.Text = "Сброс";
            this.btn_sbros.UseVisualStyleBackColor = true;
            this.btn_sbros.Click += new System.EventHandler(this.btn_sbros_Click);
            // 
            // lb_krugi
            // 
            this.lb_krugi.FormattingEnabled = true;
            this.lb_krugi.Location = new System.Drawing.Point(12, 141);
            this.lb_krugi.Name = "lb_krugi";
            this.lb_krugi.Size = new System.Drawing.Size(422, 108);
            this.lb_krugi.TabIndex = 5;
            // 
            // display_draw_timer
            // 
            this.display_draw_timer.Enabled = true;
            this.display_draw_timer.Interval = 10;
            this.display_draw_timer.Tick += new System.EventHandler(this.display_draw_timer_Tick);
            // 
            // timer
            // 
            this.timer.Interval = 1;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // P3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(446, 263);
            this.Controls.Add(this.lb_krugi);
            this.Controls.Add(this.btn_sbros);
            this.Controls.Add(this.btn_stop);
            this.Controls.Add(this.btn_start);
            this.Controls.Add(this.display);
            this.Controls.Add(this.label1);
            this.Name = "P3";
            this.Text = "P3";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label display;
        private System.Windows.Forms.Button btn_start;
        private System.Windows.Forms.Button btn_stop;
        private System.Windows.Forms.Button btn_sbros;
        private System.Windows.Forms.ListBox lb_krugi;
        private System.Windows.Forms.Timer display_draw_timer;
        private System.Windows.Forms.Timer timer;
    }
}