﻿using System;
using System.Text;

namespace pasha_g
{
    class labs
    {
        public void L2_1()
        {
            double A = 34;
            int I = -6124;
            double C = 3.2 * Math.Pow(10, 5);
            bool L = true;
            string N = "Гарбузовский";

            Console.WriteLine(string.Format("A={0}\nI={1}\nC={2}\nL={3}\nN={4}", A, I, C, L, N));
        }

        public void L2_2()
        {
            string[] in_strings;
            string out_string;
            double x_test, y_test, x_min, x_max, step, x, y;

            in_strings = System.IO.File.ReadAllLines("LAB2.TXT", Encoding.ASCII);

            x_test = Convert.ToDouble(in_strings[0]);
            y_test = Convert.ToDouble(in_strings[1]);
            x_min = Convert.ToDouble(in_strings[2]);
            x_max = Convert.ToDouble(in_strings[3]);

            step = (x_max - x_min) / 7;

            out_string = "Получено:\n";

            for(x= x_min; x<= x_max; x += step)
            {
                y = 4 * x + 1 / (x + 1);
                out_string += string.Format("для заданной функции Y({0})={1}\n", x, y);
            }

            out_string += string.Format("Составил: {0}", "Гарбузовский П");

            System.IO.File.WriteAllText("LAB2.RES", out_string);

            Console.WriteLine(out_string);
        }

        public void L3_1()
        {
            double x, y, z;
            double f;

            Console.Write("x=");
            x = Convert.ToDouble(Console.ReadLine());
            Console.Write("y=");
            y = Convert.ToDouble(Console.ReadLine());
            Console.Write("z=");
            z = Convert.ToDouble(Console.ReadLine());

            f = x * x + z;

            if(f == 0)
            {
                Console.WriteLine("Деление на ноль!");
                return;
            }

            f = Math.Min(z, Math.Max(x, y)) / f;

            Console.WriteLine("F={0}", f);
        }

        public void L3_2()
        {
            int x, y, N;

            Console.Write("x=");
            x = Convert.ToInt32(Console.ReadLine());
            Console.Write("y=");
            y = Convert.ToInt32(Console.ReadLine());

            if
            (
                (x > -12 && x < 12)
                &&
                (y > -12 && y < 12)
            )
            {
                if
                (
                    x > 0 && y > 0
                    ||
                    x < 0 && y < 0
                )
                {
                    N = 2;
                }
                else
                {
                    if(x > 0)
                    {
                        N = 3;
                    }
                    else
                    {
                        N = 1;
                    }
                }
            }
            else
            {
                N = 4;
            }

            Console.WriteLine("N={0}", N);
        }

        public void L4_1()
        {
            double a, x, y;

            for(a=15; a<=60; a += 15)
            {
                for(x=0; x<=0.5; x += 0.02)
                {
                    y = x * Math.Tan(a / 180 * Math.PI) - x * x / Math.Pow(Math.Cos(a / 180 * Math.PI), 2);
                    Console.WriteLine("y(x={0},a={1}) = {2}", x, a, y);
                }
            }
        }

        public void L4_2()
        {
            double x;
            double S, m, m1, m2, m3, m4;
            double n = 1;

            Console.Write("x=");
            x = Convert.ToDouble(Console.ReadLine());

            S = 1;

            m1 = -1;
            m2 = 1;
            m3 = 1;
            m4 = 1;

            do
            {

                m1 *= -1;
                m2 *= 2 * n - 3;        // при n=1 произведение 1 * 3 * 5 .... (2n-3)  <- получается -1
                m3 *= n * (2 * 2 * 2);
                m4 *= x - 4;

                m = m1 * (m2 / m3) * m4;

                S += m;

                n++;

            } while (m > 0.000001);

            Console.WriteLine(string.Format("y({0}) = {1}", x, S));
        }

        public void L5_1()
        {
            int[] a = { 1, 4, -3, 1, -5, -2, 7, 9999, -4, 2, 5, -53, 9999, 2 };

            int max;
            int max_index;

            int count = 0;

            max = a[0];
            max_index = 0;

            for(int i=1; i<a.Length; i++)
            {
                if(a[i] > max)
                {
                    max = a[i];
                    max_index = i;
                }
            }

            for(int i=0; i<max_index; i++)
            {
                if (a[i] < 0)
                {
                    count++;
                }
            }

            Console.WriteLine("count={0}", count);
        }

        public void L6_1()
        {
            int[,] M = new int[20,30];      // (строка, столбец)
            int[] BM = new int[30];

            Random rnd = new Random();

            Console.WriteLine("M =");

            for (int i = 0; i < 20; i++)
            {
                for (int j = 0; j < 30; j++)
                {
                    M[i, j] = 100+(int)(rnd.NextDouble()*899);
                    Console.Write("{0}  ", M[i,j]);
                }

                Console.WriteLine();
            }

            Console.WriteLine("BM =");

            for(int column=0; column< 30; column++)
            {
                int max;

                max = M[0, column];

                for(int row=0; row<20; row++)
                {
                    if(M[row, column] > max)
                    {
                        max = M[row, column];
                    }
                }

                BM[column] = max;

                Console.Write("{0}  ", max);
            }
        }

        void L7_1_TranspAndMul(ref double [,] M)
        {
            double[,] old = M;
            double[,] Mtrans;

            M = new double[old.GetLength(1), old.GetLength(0)];
            Mtrans = new double[old.GetLength(1), old.GetLength(0)];

            for (int i = 0; i < old.GetLength(0); i++)
                for (int j = 0; j < old.GetLength(1); j++)
                    Mtrans[j, i] = old[i, j];

            for(int i=0; i< old.GetLength(0); i++)
                for(int j=0;j<old.GetLength(1); j++)
                {
                    for (int k = 0; k < old.GetLength(0); k++)
                        M[j, i] += old[i, k] * Mtrans[k, j];
                }
        }

        void L7_1_WriteMatrix(ref double [,] M)
        {
            for(int i=0; i<M.GetLength(0); i++)
            {
                for (int j = 0; j < M.GetLength(1); j++)
                {
                    Console.Write("{0}\t", M[i, j]);
                }
                Console.WriteLine();
            }
        }

        public void L7_1()
        {
            double[,] B = {
                { 1,2,3,4 },
                { 5,6,7,8 },
                { 9,10,11,12 },
                { 13,14,15,16 }
            };
            double[,] D = {
                { 1,2,3 },
                { 4,5,6 },
                { 7,8,9 }
            };

            Console.WriteLine("B =");
            L7_1_WriteMatrix(ref B);

            Console.WriteLine("D =");
            L7_1_WriteMatrix(ref D);


            L7_1_TranspAndMul(ref B);
            L7_1_TranspAndMul(ref D);


            Console.WriteLine("B* =");
            L7_1_WriteMatrix(ref B);

            Console.WriteLine("D* =");
            L7_1_WriteMatrix(ref D);
        }
    }
}
