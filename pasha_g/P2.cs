﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace pasha_g
{
    public partial class P2 : Form
    {
        public P2()
        {
            InitializeComponent();
        }

        private void P2_Load(object sender, EventArgs e)
        {
            cb_material.Items.Add("пластик");       // 0
            cb_material.Items.Add("соломка");       // 1
            cb_material.Items.Add("текстиль");      // 2

            cb_material.SelectedIndex = 0;
        }

        private void vvod_parametrov(object sender, EventArgs e)
        {
            double width, height, S;
            double price, result_price;

            try
            {
                width = Convert.ToDouble(tb_width.Text);
                height = Convert.ToDouble(tb_height.Text);

                if (width < 0 || height < 0) throw new Exception("Неверное значение!");

                S = width * height;

                switch (cb_material.SelectedIndex)
                {
                    default:
                        throw new Exception("Неизвестный материал!");
                    case 0:
                        price = 300;
                        break;
                    case 1:
                        price = 450;
                        break;
                    case 2:
                        price = 250;
                        break;
                }

                result_price = price * S;

                tb_result.Text = result_price.ToString();

                status.Text = "Готов";
            }
            catch (Exception error)
            {
                status.Text = error.Message;
            }
        }
    }
}
