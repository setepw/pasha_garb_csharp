﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace pasha_g
{
    public partial class P4 : Form
    {
        public P4()
        {
            InitializeComponent();
        }

        private void P4_Load(object sender, EventArgs e)
        {
            this.Show();

            gx_context = BufferedGraphicsManager.Current;
            gx = gx_context.Allocate(display.CreateGraphics(), display.DisplayRectangle);

            btn_clear_Click(null, null);

            currentColor = Color.Black;

            run_string_value = "By Garbuzovsky PI-323";
            run_string_location = new Point(0, 0);
            run_string_font = new Font(this.Font.FontFamily, 16);

            timer_run_string.Start();
        }




        BufferedGraphics gx;
        BufferedGraphicsContext gx_context;



        bool isMouseDown;
        Color currentColor;
        Point lastMousePosition;
        Point currentMousePosition;

        string run_string_value;
        Point run_string_location;
        Font run_string_font;




        private void display_MouseDown(object sender, MouseEventArgs e)
        {
            lastMousePosition = e.Location;

            isMouseDown = true;
        }

        private void display_MouseUp(object sender, MouseEventArgs e)
        {
            isMouseDown = false;
        }

        private void display_MouseMove(object sender, MouseEventArgs e)
        {
            currentMousePosition = e.Location;

            if (isMouseDown)
            {
                display.Cursor = Cursors.UpArrow;

                gx.Graphics.DrawLine(new Pen(currentColor), lastMousePosition.X, lastMousePosition.Y, currentMousePosition.X, currentMousePosition.Y);
                gx.Render();
            }
            else
            {
                display.Cursor = Cursors.Hand;
            }

            lastMousePosition = e.Location;
        }

        private void btn_clear_Click(object sender, EventArgs e)
        {
            gx.Graphics.Clear(Color.White);
            gx.Render();
        }

        private void btn_color_Click(object sender, EventArgs e)
        {
            ColorDialog selector = new ColorDialog();

            selector.Color = currentColor;
            selector.ShowDialog();
            currentColor = selector.Color;
        }

        private void timer_run_string_Tick(object sender, EventArgs e)
        {
            SizeF str_size = gx.Graphics.MeasureString(run_string_value, run_string_font);

            gx.Graphics.FillRectangle(Brushes.Black, 0, run_string_location.Y, display.ClientRectangle.Width, run_string_location.Y + str_size.Height + 20);

            for(int i=0; i<(2 + display.ClientRectangle.Width / str_size.Width); i++)
            {
                gx.Graphics.DrawString(run_string_value, run_string_font, Brushes.White, run_string_location.X + (i* str_size.Width), run_string_location.Y + 10);
            }

            if (run_string_location.X <= -str_size.Width)
                run_string_location.X = 0;
            else
                run_string_location.X -= 1;

            gx.Render();
        }

        private void btn_grafic_Click(object sender, EventArgs e)
        {
            Size size = new Size(300, 300);
            Point position = new Point(100, 100);

            PointF[] dataset = new PointF[10];
            Random rnd = new Random();
            for (int i = 0; i < dataset.Length; i++)
                dataset[i] = new PointF((float)i / 9, (float)Math.Pow((i - 5), 2) / 25);

            gx.Graphics.FillRectangle(Brushes.White, position.X, position.Y, size.Width, size.Height);
            gx.Graphics.DrawRectangle(Pens.Black, position.X, position.Y, size.Width, size.Height);

            for (int i = 1; i < dataset.Length; i++)
                gx.Graphics.DrawLine(
                    new Pen(currentColor),
                    position.X + dataset[i].X * size.Width,
                    position.Y + dataset[i].Y * size.Height,
                    position.X + dataset[i - 1].X * size.Width,
                    position.Y + dataset[i - 1].Y * size.Height
                );

            foreach (PointF point in dataset)
            {
                gx.Graphics.FillRectangle(
                    new SolidBrush(currentColor),
                    position.X + point.X * size.Width - 3,
                    position.Y + point.Y * size.Height - 3,
                    6, 6
                );

                string label = string.Format("({0}; {1})", point.X.ToString("0.00"), point.Y.ToString("0.00"));
                Font label_font = new Font(this.Font.FontFamily, 7);
                SizeF label_size = gx.Graphics.MeasureString(label, label_font);

                gx.Graphics.DrawString(
                    label,
                    label_font,
                    Brushes.Gray,
                    position.X + point.X * size.Width - label_size.Width/2,
                    position.Y + point.Y * size.Height + 6
                );
            }
        }

        private void btn_diargamma_Click(object sender, EventArgs e)
        {
            Size size = new Size(300, 300);
            Point position = new Point(100, 100);

            Dictionary<string, float> dataset = new Dictionary<string, float>();
            dataset.Add("Россия", 1);
            dataset.Add("Украина", 0.6F);
            dataset.Add("Белорусия", 0.3F);
            dataset.Add("Австралия", 0.1F);
            dataset.Add("Англия", 0.09F);
            dataset.Add("США", 0.01F);

            gx.Graphics.FillRectangle(Brushes.White, position.X, position.Y, size.Width, size.Height);
            gx.Graphics.DrawRectangle(Pens.Black, position.X, position.Y, size.Width, size.Height);

            int item_number = 0;
            foreach (var item in dataset)
            {
                gx.Graphics.FillRectangle(
                    new SolidBrush(currentColor),
                    position.X + item_number * (size.Width / dataset.Count),
                    position.Y + size.Height - size.Height * item.Value,
                    (int)((size.Width / dataset.Count) * 0.8),
                    size.Height * item.Value
                );

                string label = string.Format("{0}", item.Key);
                Font label_font = new Font(this.Font.FontFamily, 7);
                SizeF label_size = gx.Graphics.MeasureString(label, label_font);

                gx.Graphics.DrawString(
                    label,
                    label_font,
                    Brushes.Gray,
                    position.X + item_number * (size.Width / dataset.Count) + (int)(size.Width * 0.4 / dataset.Count) - label_size.Width / 2,
                    position.Y + size.Height - size.Height * item.Value - label_size.Height - 4
                );

                item_number++;
            }
        }
    }
}
