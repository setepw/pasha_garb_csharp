﻿namespace pasha_g
{
    partial class P2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_height = new System.Windows.Forms.TextBox();
            this.tb_width = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cb_material = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_result = new System.Windows.Forms.TextBox();
            this.status = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Жалюзи";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Высота (м)";
            // 
            // tb_height
            // 
            this.tb_height.Location = new System.Drawing.Point(17, 64);
            this.tb_height.Name = "tb_height";
            this.tb_height.Size = new System.Drawing.Size(255, 20);
            this.tb_height.TabIndex = 2;
            this.tb_height.TextChanged += new System.EventHandler(this.vvod_parametrov);
            // 
            // tb_width
            // 
            this.tb_width.Location = new System.Drawing.Point(17, 103);
            this.tb_width.Name = "tb_width";
            this.tb_width.Size = new System.Drawing.Size(255, 20);
            this.tb_width.TabIndex = 4;
            this.tb_width.TextChanged += new System.EventHandler(this.vvod_parametrov);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Ширина (м)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Материал";
            // 
            // cb_material
            // 
            this.cb_material.FormattingEnabled = true;
            this.cb_material.Location = new System.Drawing.Point(17, 142);
            this.cb_material.Name = "cb_material";
            this.cb_material.Size = new System.Drawing.Size(255, 21);
            this.cb_material.TabIndex = 6;
            this.cb_material.SelectedIndexChanged += new System.EventHandler(this.vvod_parametrov);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 188);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Стоимость";
            // 
            // tb_result
            // 
            this.tb_result.Location = new System.Drawing.Point(17, 204);
            this.tb_result.Name = "tb_result";
            this.tb_result.Size = new System.Drawing.Size(255, 20);
            this.tb_result.TabIndex = 8;
            // 
            // status
            // 
            this.status.AutoSize = true;
            this.status.Location = new System.Drawing.Point(14, 239);
            this.status.Name = "status";
            this.status.Size = new System.Drawing.Size(36, 13);
            this.status.TabIndex = 9;
            this.status.Text = "Готов";
            // 
            // P2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.status);
            this.Controls.Add(this.tb_result);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cb_material);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tb_width);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tb_height);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "P2";
            this.Text = "P2";
            this.Load += new System.EventHandler(this.P2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_height;
        private System.Windows.Forms.TextBox tb_width;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cb_material;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tb_result;
        private System.Windows.Forms.Label status;
    }
}