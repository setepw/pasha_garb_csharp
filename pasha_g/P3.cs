﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace pasha_g
{
    public partial class P3 : Form
    {
        public P3()
        {
            InitializeComponent();
        }

        private void P3_Load(object sender, EventArgs e)
        {
            btn_sbros_Click(null, null);

            display_draw_timer.Start();
        }




        private DateTime time_on_start;
        private TimeSpan time_inteval;



        private void btn_start_Click(object sender, EventArgs e)
        {
            if (timer.Enabled)
            {
                lb_krugi.Items.Add(time_inteval.ToString());
            }
            else
            {
                time_on_start = DateTime.Now;
                timer.Start();
            }
        }

        private void btn_stop_Click(object sender, EventArgs e)
        {
            timer.Stop();
        }

        private void btn_sbros_Click(object sender, EventArgs e)
        {
            time_on_start = DateTime.Now;
            time_inteval = new TimeSpan();

            lb_krugi.Items.Clear();
        }




        private void timer_Tick(object sender, EventArgs e)
        {
            time_inteval += DateTime.Now - time_on_start;
            time_on_start = DateTime.Now;
        }




        private void display_draw_timer_Tick(object sender, EventArgs e)
        {
            display.Text = time_inteval.ToString();
        }

    }
}
