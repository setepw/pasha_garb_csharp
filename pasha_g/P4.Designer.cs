﻿namespace pasha_g
{
    partial class P4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_diargamma = new System.Windows.Forms.Button();
            this.btn_grafic = new System.Windows.Forms.Button();
            this.btn_clear = new System.Windows.Forms.Button();
            this.btn_color = new System.Windows.Forms.Button();
            this.display = new System.Windows.Forms.Panel();
            this.timer_run_string = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btn_diargamma);
            this.panel1.Controls.Add(this.btn_grafic);
            this.panel1.Controls.Add(this.btn_clear);
            this.panel1.Controls.Add(this.btn_color);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(98, 445);
            this.panel1.TabIndex = 0;
            // 
            // btn_diargamma
            // 
            this.btn_diargamma.Location = new System.Drawing.Point(12, 123);
            this.btn_diargamma.Name = "btn_diargamma";
            this.btn_diargamma.Size = new System.Drawing.Size(75, 23);
            this.btn_diargamma.TabIndex = 2;
            this.btn_diargamma.Text = "Диаграмма";
            this.btn_diargamma.UseVisualStyleBackColor = true;
            this.btn_diargamma.Click += new System.EventHandler(this.btn_diargamma_Click);
            // 
            // btn_grafic
            // 
            this.btn_grafic.Location = new System.Drawing.Point(12, 94);
            this.btn_grafic.Name = "btn_grafic";
            this.btn_grafic.Size = new System.Drawing.Size(75, 23);
            this.btn_grafic.TabIndex = 2;
            this.btn_grafic.Text = "График";
            this.btn_grafic.UseVisualStyleBackColor = true;
            this.btn_grafic.Click += new System.EventHandler(this.btn_grafic_Click);
            // 
            // btn_clear
            // 
            this.btn_clear.Location = new System.Drawing.Point(12, 41);
            this.btn_clear.Name = "btn_clear";
            this.btn_clear.Size = new System.Drawing.Size(75, 23);
            this.btn_clear.TabIndex = 1;
            this.btn_clear.Text = "Отчистить";
            this.btn_clear.UseVisualStyleBackColor = true;
            this.btn_clear.Click += new System.EventHandler(this.btn_clear_Click);
            // 
            // btn_color
            // 
            this.btn_color.Location = new System.Drawing.Point(12, 12);
            this.btn_color.Name = "btn_color";
            this.btn_color.Size = new System.Drawing.Size(75, 23);
            this.btn_color.TabIndex = 0;
            this.btn_color.Text = "Цвет";
            this.btn_color.UseVisualStyleBackColor = true;
            this.btn_color.Click += new System.EventHandler(this.btn_color_Click);
            // 
            // display
            // 
            this.display.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.display.Cursor = System.Windows.Forms.Cursors.SizeNS;
            this.display.Dock = System.Windows.Forms.DockStyle.Fill;
            this.display.Location = new System.Drawing.Point(98, 0);
            this.display.Name = "display";
            this.display.Size = new System.Drawing.Size(562, 445);
            this.display.TabIndex = 1;
            this.display.MouseDown += new System.Windows.Forms.MouseEventHandler(this.display_MouseDown);
            this.display.MouseMove += new System.Windows.Forms.MouseEventHandler(this.display_MouseMove);
            this.display.MouseUp += new System.Windows.Forms.MouseEventHandler(this.display_MouseUp);
            // 
            // timer_run_string
            // 
            this.timer_run_string.Interval = 10;
            this.timer_run_string.Tick += new System.EventHandler(this.timer_run_string_Tick);
            // 
            // P4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(660, 445);
            this.Controls.Add(this.display);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "P4";
            this.Text = "P4";
            this.Load += new System.EventHandler(this.P4_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_diargamma;
        private System.Windows.Forms.Button btn_grafic;
        private System.Windows.Forms.Button btn_clear;
        private System.Windows.Forms.Button btn_color;
        private System.Windows.Forms.Panel display;
        private System.Windows.Forms.Timer timer_run_string;
    }
}